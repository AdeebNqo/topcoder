import java.util.Vector;
import java.util.Arrays;
public class RouteIntersection{

	Vector<cordinate> made_moves;
	cordinate prev_move;
	
	public RouteIntersection(){
		
	}
	public String isValid(int N, int[] coords, String moves){
		
		prev_move = new cordinate(N); //initial move
		made_moves = new Vector<cordinate>();
		made_moves.add(prev_move);
		String[] plusminus = moves.split("");
		
		for (int i=0;i<coords.length;i++){
			if (plusminus[i].equals("-")){
				//increment i'th pos
				cordinate tmp = prev_move.getNext(coords[i]);
				prev_move.decrement(coords[i]);
				//check if tmp is unique, if it's not -- break
				if (made_moves.indexOf(tmp)!=-1) return "NOT VALID";
				//create new prev and add it to vector
				prev_move = tmp;
				made_moves.add(prev_move);
			}
			else{
				//increment i'th pos
				cordinate tmp = prev_move.getPrev(coords[i]);
				prev_move.increment(coords[i]);
				//check if tmp is unique, if it's not -- break
				if (made_moves.indexOf(tmp)!=-1) return "NOT VALID";
				//create new prev and add it to vector
				prev_move = tmp;
				made_moves.add(prev_move);
			}
		}
		return "VALID";
	}
	//inner class for representing coordinates
	public class cordinate{
		int[] val;
		int dim;
		public cordinate(int dim){
				this.dim = dim;
				val = new int[dim];
		}
		public cordinate(int[] newcords){
			val = newcords;
		}
		//returns another cordinate by incrementing i'th entry
		public cordinate getNext(int i){
				val[i] +=1;
				return new cordinate(val);
		}
		public void decrement(int i){
			val[i] -=1;
		}
		//returns another cordinate by decerementing i'th entry
		public cordinate getPrev(int i){
				val[i] -= 1;
				return new cordinate(val);
		}
		public void increment(int i){
			val[i] += 1;
		}
		public String toString(){
			return Arrays.toString(val);
		}
	}
}
